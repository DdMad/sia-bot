// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('ionicApp', ['ionic'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .constant('API_ENDPOINT', {
    host: 'http://127.0.0.1',
    // Below is our openshift server address
    // host: 'http://sia-bot-sia-bot.44fs.preview.openshiftapps.com',
    port: 5000
  })

  // All this does is allow the message
  // to be sent when you tap return
  .directive('input', function ($timeout) {
    return {
      restrict: 'E',
      scope: {
        'returnClose': '=',
        'onReturn': '&',
        'onFocus': '&',
        'onBlur': '&'
      },
      link: function (scope, element, attr) {
        element.bind('focus', function (e) {
          if (scope.onFocus) {
            $timeout(function () {
              scope.onFocus();
            });
          }
        });
        element.bind('blur', function (e) {
          if (scope.onBlur) {
            $timeout(function () {
              scope.onBlur();
            });
          }
        });
        element.bind('keydown', function (e) {
          if (e.which == 13) {
            if (scope.returnClose) element[0].blur();
            if (scope.onReturn) {
              $timeout(function () {
                scope.onReturn();
              });
            }
          }
        });
      }
    }
  })


  .controller('Messages', function ($scope, $timeout, $ionicScrollDelegate, $http, API_ENDPOINT) {
    var apiEndpoint = API_ENDPOINT.host + ':' + API_ENDPOINT.port;
    var sitaApiKey = '83202136355d1704350fb3ef596ac126';
    // SIA should have these info if user has logged in.
    var personalInfo = {
      emailAddress: 'ddmad1030@gmail.com',
      mobileNumber: '+12345678',
      firstName: 'San',
      lastName: 'Zhang',
      title: 'MR',

    };
    $scope.onFlightClick = function (flight) {
      var message = `Processing your booking for flight ${flight.Carrier.Code + flight.FlightNumber} on ${flight.FlightDate}`
      $scope.messages.push({
        text: message,
        time: Date.now()
      })
      $timeout(function () {
        $scope.messages.push({
          text: 'I have booked the flight for you',
          time: Date.now()
        })
      }, 3000);
      console.log(flight);
    }
    $scope.hideTime = true;
    $scope.request = {};

    var alternate,
      isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

    $scope.sendMessage = function () {
      alternate = !alternate;

      var d = new Date();
      d = d.toLocaleTimeString().replace(/:\d+ /, ' ');

      $scope.messages.push({
        // userId: alternate ? '54321' : '12345',
        userId: "123",
        text: $scope.data.message,
        time: d
      });

      console.log($scope.data.message);
      var data = { 'message': $scope.data.message }
      if ($scope.ctx) { data.contexts = $scope.ctx; }

      // POST query (message) call
      $http.post(`${apiEndpoint}/flight`, data).success(function (response) {
        console.log(response);
        $scope.messages.push({
          // userId: alternate ? '54321' : '12345',
          userId: "321",
          text: response.response.result.fulfillment.speech,
          time: response.response.timestamp
        });
        $scope.ctx = response.response.result.contexts;
        $scope.request.type = response.response.result.parameters.bookingType || response.response.result.parameters.bookType;
        var parameters = response.response.result.parameters;
        $scope.request.complete = parameters.depart !== '' && parameters.dest !== '' && parameters.start !== '';
        if(parameters.filter && parameters.filter === 'noon'){
          var flight = $scope.lastRetrievedFlights.filter(f => f.Id == 3);
          console.log(flight);
          message = {
              header: `Flight from ${flight[0].OriginStation.Name} to ${flight[0].DestinationStation.Name}`,
              subheader: `On ` + flight[0].FlightDate,
              body: flight,
              isList: true
            }
          $scope.messages.push(message);
        }
        
        if ($scope.request.type === 'flight' && $scope.request.complete && !parameters.filter) {
          // query flight API call
          var url = `${apiEndpoint}/get_flight_list/`;
          if (parameters.dest.toLowerCase() === 'beijing') {
            url += 'toBJ'
          } else if (parameters.dest.toLowerCase() === 'kl' || parameters.dest.toLowerCase() === 'kuala lumpur') {
            url += 'toKL'
          }
          $http.get(url).success(function (response) {
            console.log(response);
            $scope.lastRetrievedFlights = response.Segments;
            response.Segments.map(s => { s.FlightDate = parameters.start });
            $scope.messages.push({
              header: `Flight from ${parameters.depart} to ${parameters.dest}`,
              subheader: `On ${parameters.start}`,
              body: response.Segments,
              isList: true
            }, {
                text: 'Please tap to book the flight',
                time: Date.now()
              })
          }).error(function (error) {
            console.log(error);
          });
        } else if ($scope.request.type === 'checkIn') {
          // online checkin generate boarding pass API call
          $http.get(`${apiEndpoint}/check_in`).success(function (response) {
            console.log(response);
            var xml = '<Request LanguageCode="en" emailAddress="' + personalInfo.emailAddress +
              '" mobileNumber="' + personalInfo.mobileNumber +
              '" gate="' + response.gate +
              '" boardingHHMM="' + response.boardingHHMM +
              '" departHHMM="' + response.departHHMM +
              '" arriveHHMM="' + response.arriveHHMM +
              '" ffMiles="' + response.ffMiles +
              '" DepartTerminal="' + response.DepartTerminal +
              '" ffTier="' + response.ffTier +
              '" message="' + + response.message +
              '" CabinName="' + response.CabinName +
              '" > <Barcode firstName="' + personalInfo.firstName +
              '" lastName="' + personalInfo.lastName +
              '" title="' + personalInfo.title +
              '" bookingRef="' + response.bookingRef +
              '" depAirportCode="' + response.depAirportCode +
              '" arrAirportCode="' + response.arrAirportCode +
              '" carrier="' + response.carrier +
              '" flightNumber="' + response.flightNumber +
              '" depDate="' + response.depDate +
              '" classCode="' + response.classCode +
              '" seatNumber="' + response.seatNumber +
              '" seqNumber="' + response.seqNumber +
              '" ffAirline="' + response.ffAirline +
              '" ffNumber="' + response.ffNumber +
              '" ticketNumber="' + response.ticketNumber +
              '" issuingCarrier="' + response.issuingCarrier + '" /> </Request>';
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", "https://dev2dbp.api.aero/api/ZZ", true);
            xmlhttp.setRequestHeader("Content-Type", "application/xml");
            xmlhttp.setRequestHeader("x-apiKey", sitaApiKey);
            xmlhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
            xmlhttp.send(xml);
            // console.log(escape(xml));
            var d = new Date();
            d = d.toLocaleTimeString().replace(/:\d+ /, ' ');
            $scope.messages.push({
              // userId: alternate ? '54321' : '12345',
              userId: "321",
              text: "Your boarding pass has sent to your email",
              time: d
            });
          }).error(function (error) {
            console.log(error);
          });
        } else if ($scope.request.type === 'hotel' && $scope.request.complete) {
          // TODO: query hotel API call (future development)
        }
      }).error(function (error) {
        console.log(error);
      });

      delete $scope.data.message;
      $ionicScrollDelegate.scrollBottom(true);
    };

    $scope.record = function () {
      var recognition = new webkitSpeechRecognition(); // To Device
      recognition.lang = 'en-US';

      recognition.onresult = function (event) {
        if (event.results.length > 0) {
          console.log("Get something!");
          console.log(event.results[0][0].transcript);
          $scope.data.message = event.results[0][0].transcript;
          $scope.$apply();
        }
      };

      recognition.start();
    }

    $scope.inputUp = function () {
      if (isIOS)
        $scope.data.keyboardHeight = 216;
      $timeout(function () {
        $ionicScrollDelegate.scrollBottom(true);
      }, 300);

    };

    $scope.inputDown = function () {
      if (isIOS)
        $scope.data.keyboardHeight = 0;
      $ionicScrollDelegate.resize();
    };

    $scope.closeKeyboard = function () {
      // cordova.plugins.Keyboard.close();
    };


    $scope.data = {};
    $scope.myId = '123';
    $scope.messages = [];

  });