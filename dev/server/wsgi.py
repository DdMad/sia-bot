#!flask/bin/python
from flask import Flask, jsonify, abort, request
from flask_cors import CORS, cross_origin
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import json
import unidecode
import os.path
import sys
import requests

try:
    import apiai
except ImportError:
    sys.path.append(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    import apiai

CLIENT_ACCESS_TOKEN = '01a7fd88302f4833981bb4c0ff759954'

SKY_SCANNER_API_KEY = "nu624469115227645468138187153897"
SKY_SCANNER_POST_ADDR = "http://partners.api.skyscanner.net/apiservices/pricing/v1.0"

application = Flask(__name__)
CORS(application, resources={r"/*": {"origins": "*"}})

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]


@application.route("/")
def hello():
    return "Hello World!"


@application.route('//todo/api/v1.0/tasks', methods=['GET'])
def get_tasks():
    return jsonify({'tasks': tasks})


@application.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    return jsonify({'task': task[0]})


@application.route('/todo/api/v1.0/tasks', methods=['POST'])
def create_task():
    if not request.get_json() or not 'title' in request.get_json():
        abort(400)
    task = {
        'id': tasks[-1]['id'] + 1,
        'title': request.get_json()['title'],
        'description': request.get_json().get('description', ""),
        'done': False
    }
    tasks.append(task)
    return jsonify({'task': task}), 201 \
 \
           @ application.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['PUT'])


def update_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    if not request.json:
        abort(400)
    if 'title' in request.json and type(request.json['title']) != unidecode:
        abort(400)
    if 'description' in request.json and type(request.json['description']) is not unidecode:
        abort(400)
    if 'done' in request.json and type(request.json['done']) is not bool:
        abort(400)
    task[0]['title'] = request.json.get('title', task[0]['title'])
    task[0]['description'] = request.json.get('description', task[0]['description'])
    task[0]['done'] = request.json.get('done', task[0]['done'])
    return jsonify({'task': task[0]})


@application.route('/todo/api/v1.0/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    task = [task for task in tasks if task['id'] == task_id]
    if len(task) == 0:
        abort(404)
    tasks.remove(task[0])
    return jsonify({'result': True})


@application.route('/flight', methods=['POST'])
def book_flight():
    application.logger.info(request.get_json())

    if not request.get_json() or not 'message' in request.get_json():
        abort(400)

    message = request.get_json()['message']

    contexts = request.get_json()['contexts'] if 'contexts' in request.get_json() else []

    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    ai_request = ai.text_request()

    ai_request.lang = 'en'  # optional, default value equal 'en'

    # request.session_id = "<SESSION ID, UNIQUE FOR EACH USER>"

    ai_request.query = message
    ai_request.contexts = contexts

    response = ai_request.getresponse()
    # response.headers['Access-Control-Allow-Origin'] = '*'

    # application.logger.info(dumps(response.read()));

    return jsonify({'response': json.loads(response.read().decode('utf-8'))}), 201


@application.route('/get_flight_list/<string:flight_list_id>', methods=['GET'])
def get_flight_list(flight_list_id):
    flight_list_data1 = {
        "Segments": [
            {
                "Id": 1,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 15277,
                    "ParentId": 996,
                    "Code": "PEK",
                    "Type": "Airport",
                    "Name": "Beijing Capital"
                },
                "DepartureDateTime": "2016-12-01T01:10:00",
                "ArrivalDateTime": "2016-12-01T07:15:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "Duration": 365,
                "FlightNumber": "800",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 674.1,
                "Currency": "SGD"
            },
            {
                "Id": 2,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 15277,
                    "ParentId": 996,
                    "Code": "PEK",
                    "Type": "Airport",
                    "Name": "Beijing Capital"
                },
                "DepartureDateTime": "2016-12-02T01:10:00",
                "ArrivalDateTime": "2016-12-02T07:15:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "Duration": 365,
                "FlightNumber": "800",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 684.1,
                "Currency": "SGD"
            },
            {
                "Id": 3,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 15277,
                    "ParentId": 996,
                    "Code": "PEK",
                    "Type": "Airport",
                    "Name": "Beijing Capital"
                },
                "DepartureDateTime": "2016-12-03T01:10:00",
                "ArrivalDateTime": "2016-12-03T07:15:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "Duration": 365,
                "FlightNumber": "800",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 660.1,
                "Currency": "SGD"
            }
        ]
    }

    flight_list_data2 = {
        "Segments": [
            {
                "Id": 1,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "08:30:00",
                "ArrivalDateTime": "09:30:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1417,
                    "Code": "MI",
                    "Name": "SilkAir",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/MI.png"
                },
                "Duration": 60,
                "FlightNumber": "5304",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 522.6,
                "Currency": "SGD"
            },
            {
                "Id": 2,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "10:15:00",
                "ArrivalDateTime": "11:15:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1417,
                    "Code": "MI",
                    "Name": "SilkAir",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/MI.png"
                },
                "Duration": 60,
                "FlightNumber": "5426",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 522.6,
                "Currency": "SGD"
            },
            {
                "Id": 3,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "12:45:00",
                "ArrivalDateTime": "13:45:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1417,
                    "Code": "MI",
                    "Name": "SilkAir",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/MI.png"
                },
                "Duration": 60,
                "FlightNumber": "5118",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 612.6,
                "Currency": "SGD"
            },
            {
                "Id": 4,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "15:25:00",
                "ArrivalDateTime": "16:25:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1417,
                    "Code": "MI",
                    "Name": "SilkAir",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/MI.png"
                },
                "Duration": 60,
                "FlightNumber": "5430",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 612.6,
                "Currency": "SGD"
            },
            {
                "Id": 5,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "16:10:00",
                "ArrivalDateTime": "17:10:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1417,
                    "Code": "MI",
                    "Name": "SilkAir",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/MI.png"
                },
                "Duration": 60,
                "FlightNumber": "5330",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 612.6,
                "Currency": "SGD"
            },
            {
                "Id": 6,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "18:45:00",
                "ArrivalDateTime": "19:45:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "Duration": 60,
                "FlightNumber": "118",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 522.6,
                "Currency": "SGD"
            },
            {
                "Id": 7,
                "OriginStation": {
                    "Id": 16292,
                    "ParentId": 7101,
                    "Code": "SIN",
                    "Type": "Airport",
                    "Name": "Singapore Changi"
                },
                "DestinationStation": {
                    "Id": 13311,
                    "ParentId": 4350,
                    "Code": "KUL",
                    "Type": "Airport",
                    "Name": "Kuala Lumpur International"
                },
                "DepartureDateTime": "20:00:00",
                "ArrivalDateTime": "20:55:00",
                "Carrier": {
                    "Id": 1713,
                    "Code": "SQ",
                    "Name": "Singapore Airlines",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/SQ.png"
                },
                "OperatingCarrier": {
                    "Id": 1417,
                    "Code": "MI",
                    "Name": "SilkAir",
                    "ImageUrl": "http://s1.apideeplink.com/images/airlines/MI.png"
                },
                "Duration": 55,
                "FlightNumber": "5342",
                "JourneyMode": "Flight",
                "Directionality": "Outbound",
                "Price": 612.6,
                "Currency": "SGD"
            }
        ]
    }

    flight_list = dict()
    flight_list["toBJ"] = flight_list_data1
    flight_list["toKL"] = flight_list_data2
    return jsonify(flight_list[flight_list_id])


@application.route("/check_in", methods=["GET"])
def check_in():
    flight_data = {
        "gate": "212",
        "boardingHHMM": "12:00",
        "departHHMM": "12:45",
        "arriveHHMM": "13:45",
        "ffMiles": "",
        "DepartTerminal": "Term 2",
        "ffTier": "",
        "message": "Sample",
        "CabinName": "Business",
        "bookingRef": "PNR123",
        "depAirportCode": "SIN",
        "arrAirportCode": "KUL",
        "carrier": "SQ",
        "flightNumber": "5118",
        "depDate": "2016-10-9",
        "classCode": "Y",
        "seatNumber": "21B",
        "seqNumber": "002",
        "ffAirline": "ZZ",
        "ffNumber": "123456789",
        "ticketNumber": "000123456789012",
        "issuingCarrier": "SQ"
    }
    return jsonify(flight_data)


@application.route('/flight_info', methods=['GET'])
def get_flights():
    country = "SG"
    currency = "SGD"
    locale = "en-GB"
    originplace = "SIN"
    destinationplace = "PEK-sky"
    outbounddate = "2016-12-01"
    adults = 1
    cabinclass = "Economy"

    post_fields = {
        'apiKey': SKY_SCANNER_API_KEY,
        'country': country,
        'currency': currency,
        'locale': locale,
        'originplace': originplace,
        'destinationplace': destinationplace,
        'outbounddate': outbounddate,
        'adults': adults,
        'cabinclass': cabinclass
    }

    create_session_req = requests.post(SKY_SCANNER_POST_ADDR, json=post_fields)

    application.logger.info(create_session_req.url)
    application.logger.info(create_session_req.text)
    application.logger.info(create_session_req.headers)

    polling_session_req = requests.get(create_session_req.url, params={'apiKey': SKY_SCANNER_API_KEY})

    application.logger.info(polling_session_req.content)

    return polling_session_req.content


if __name__ == '__main__':
    application.run(debug=True)
