# Chatbot for SIA
This is a prototype about a chatbot named "Sia" and designed for Singapore Airlines Mobile App.

## Tech Stack
### Client
[ionic framework](http://ionicframework.com/), [AngularJS](https://angularjs.org/), [Sass](http://sass-lang.com/)

### Server
[Python](https://www.python.org/), [Flask framework](http://flask.pocoo.org/), [OpenShift from Redhat](https://www.openshift.com/)

### Other APIs
[API.AI](https://api.ai/), [Skyscanner Flight API](http://en.business.skyscanner.net/), [SITA Boarding Pass API](http://www.sita.aero/)

## Setup Local Server
The server is written in **[Python](https://www.python.org/)** and uses **[Flask](http://flask.pocoo.org/)** framework. The required python library are contained in requirements.txt.

Install all the dependencies by running this command:
```bash
pip install -r requirements.txt
```

Then go to the server directory by:
```bash
cd server
```

Then run the server by:
```bash
python wsgi.py
```

Then the server should run on `http://locahost:5000`

## Deploy to OpenShift
Please refer to **[OpenShift (Next Gen) Web Console Documentation](https://docs.openshift.com/online/getting_started/basic_walkthrough.html)** to create a new python 3.5 project.
After the project is successful built, Go to **Applications > Deployment > [project] > Environment**
Add an environment variable `APP_MODULE = dev.server.wsgi`. Then depoly the project.

Currently the porject is using local server. If you want to deploy the server to OpenShift, change the host to your OpenShift server address in `app.js` in `line 27` (e.g. `http://sia-bot-sia-bot.44fs.preview.openshiftapps.com`)

## Run Client
Install ionic and cordova:
```bash
npm install -g cordova ionic
```

Download the code and go to the client directory:
```bash
cd client
```

Then install dependencies by:
```bash
npm install
```

Then run the ionic app by:
```bash
ionic serve
```

Then your browser will open a new window and run the app at `http://localhost:8100`




